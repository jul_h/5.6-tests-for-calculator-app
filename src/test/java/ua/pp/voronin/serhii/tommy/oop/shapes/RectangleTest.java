package ua.pp.voronin.serhii.tommy.oop.shapes;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RectangleTest {
    private Rectangle testRectangle = new Rectangle(4, 6);

    @Test(expected = IllegalStateException.class)
    public void failToCreateRectangleWithNegativeSideA() {
        new Rectangle(-4, 6);
    }

    @Test(expected = IllegalStateException.class)
    public void failToCreateRectangleWithNegativeSideB() {
        new Rectangle(4, -6);
    }

    @Test(expected = IllegalStateException.class)
    public void failToCreateRectangleWithZeroSideA() {
        new Rectangle(0, 6);
    }

    @Test(expected = IllegalStateException.class)
    public void failToCreateRectangleWithZeroSideB() {
        new Rectangle(4, 0);
    }

    @Test
    public void checkAreaCalculation() {
        assertEquals(24, testRectangle.getArea(), 0.0);
    }

    @Test
    public void setSideAWorksProperly() {
        testRectangle.setSideA(7);
    }

    @Test(expected = IllegalStateException.class)
    public void setSideAToZeroThrowsException() {
        testRectangle.setSideA(0);
    }

    @Test(expected = IllegalStateException.class)
    public void setSideAToNegativeThrowsException() {
        testRectangle.setSideA(-4);
    }

    @Test
    public void setSideBWorksProperly() {
        testRectangle.setSideB(7);
    }

    @Test(expected = IllegalStateException.class)
    public void setSideBToZeroThrowsException() {
        testRectangle.setSideB(0);
    }

    @Test(expected = IllegalStateException.class)
    public void setSideBToNegativeThrowsException() {
        testRectangle.setSideB(-4);
    }

    @Test
    public void testNameReturnsProperName() {
        assertEquals("прямокутник", testRectangle.getName());
    }
}


