package ua.pp.voronin.serhii.tommy.oop.shapes;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ShapeTest {
    @Test
    public void toStringReturnsExpectedString() {
        Circle circle = new Circle(5);
        String expectedOutput = "[Фігура: коло площею 78.53981633974483]";
        assertEquals(expectedOutput, circle.toString());

        Rectangle rectangle = new Rectangle(4, 6);
        expectedOutput = "[Фігура: прямокутник площею 24.0]";
        assertEquals(expectedOutput, rectangle.toString());

        Square square = new Square(5);
        expectedOutput = "[Фігура: квадрат площею 25.0]";
        assertEquals(expectedOutput, square.toString());
    }
}
