package ua.pp.voronin.serhii.tommy.oop.shapes;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CircleTest {
    private Circle testCircle = new Circle(5);

    @Test(expected = IllegalStateException.class)
    public void failToCreateCircleWithNegativeRadius() {
        new Circle(-5);
    }

    @Test(expected = IllegalStateException.class)
    public void failToCreateCircleWithZeroRadius() {
        new Circle(0);
    }

    @Test
    public void checkAreaCalculation() {
        assertEquals(Math.PI * 25, testCircle.getArea(), 0.0);
    }

    @Test
    public void setRadiusWorksProperly() {
        testCircle.setRadius(7);
    }

    @Test(expected = IllegalStateException.class)
    public void setRadiusToZeroThrowsException() {
        testCircle.setRadius(0);
    }

    @Test(expected = IllegalStateException.class)
    public void setRadiusToNegativeThrowsException() {
        testCircle.setRadius(-4);
    }

    @Test
    public void testNameReturnsProperName() {
        assertEquals("коло", testCircle.getName());
    }
}
