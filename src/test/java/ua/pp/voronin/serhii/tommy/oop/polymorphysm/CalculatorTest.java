package ua.pp.voronin.serhii.tommy.oop.polymorphysm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ua.pp.voronin.serhii.tommy.oop.shapes.Shape;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorTest {
    private static final double SAMPLE_AREA = 15;
    @Mock
    private Shape mockShape;

    @Test
    public void verifyShapeAreaIsNotChanged() {
        when(mockShape.getArea()).thenReturn(SAMPLE_AREA);
        double area = Calculator.calculateArea(mockShape);
        assertEquals(SAMPLE_AREA, area, 0.0);
        }

    @Test(expected = IllegalStateException.class)
    public void verifyNullShapeThrowsIllegalStateException() {

        Calculator.calculateArea(null);

    }
}
