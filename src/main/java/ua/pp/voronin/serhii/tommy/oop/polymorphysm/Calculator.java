package ua.pp.voronin.serhii.tommy.oop.polymorphysm;

import ua.pp.voronin.serhii.tommy.oop.shapes.Circle;
import ua.pp.voronin.serhii.tommy.oop.shapes.Rectangle;
import ua.pp.voronin.serhii.tommy.oop.shapes.Shape;
import ua.pp.voronin.serhii.tommy.oop.shapes.Square;

public class Calculator {

    public static double calculateArea(Shape/* тут треба вірно оголосити параметри */ shape) {
        if (shape == null) {
            throw new IllegalStateException("Shape cannot be null");
        }
        double area = shape.getArea()/* тут треба отримати площу */;
        System.out.println("Площа об'єкту " + shape + " складає " + area);
        return area;
    }
}
